# This base image contains Quarto, LaTeX, LibreOffice Writer and popular R and Python packages
FROM registry.git.embl.de/grp-bio-it/gitlab-images/template-quarto:latest

# If you need to install additional r packages - replace r-tidyverse by the list of packages
# RUN micromamba install -y -n base -c conda-forge r-tidyverse \
#     && micromamba clean --all --yes \
#     && rm -rf /opt/conda/conda-meta