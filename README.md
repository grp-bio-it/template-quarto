# GitLab template - Quarto

This template was developed by [Bio-IT community](https://bio-it.embl.de/) under the [Open Software License](/LICENSE.md).
There is at the moment no reference material for how to customize this template.

You can simply modify `index.qmd` or create additional Quarto files.
The folder is considered a project and files will be rendered automatically.

**Pushing** this project to its remote repository triggers a **pipeline** to generate a static website from the 
**Quarto** files in the main folder, converted to HTML in `public`. 
These instructions are specified in the `.gitlab-ci.yml` file.

Additionally, the process uses a Quarto environment that can be further customized by modifying the `Dockerfile`.

Learn more about [GitLab Pages](https://about.gitlab.com/product/pages/) by consulting the official
[documentation](https://docs.gitlab.com/ee/user/project/pages/).
